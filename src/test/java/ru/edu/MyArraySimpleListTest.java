package ru.edu;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MyArraySimpleListTest {

    private static final String FIRST = "First";
    private static final String SECOND = "Second";
    private static final String THIRD = "Third";
    private static final String FOURTH = "Fourth";
    private SimpleList<String> list;
    private SimpleList<String> listCapacity;

    @Before
    public void setUp() throws Exception {
        list = new MyArraySimpleList<>();
        listCapacity = new MyArraySimpleList<>(30);
    }

    @Test
    public void removeTest() {
        list.add(FIRST);
        list.add(SECOND);
        list.add(THIRD);
        list.add(FOURTH);
        list.remove(0); // удаление первого элемента
        assertEquals(SECOND, list.get(0));
        assertEquals(3, list.size());

        list.remove(2); // удаление последнего элемента
        assertEquals(THIRD, list.get(list.size() - 1));
        assertEquals(2, list.size());

        list.add(FOURTH);

        list.remove(1); // удаление элемента из середины
        assertEquals(2, list.size());

        list.remove(0); // удаление первого элемента
        list.remove(0); // удаление оставшегося элемента, список становится пустым
        assertEquals(0, list.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void exceptionRemoveTest() {
        list.remove(30);
    }

    @Test
    public void listCapacityTest() {
        listCapacity.add(FIRST);
        assertEquals(FIRST, listCapacity.get(0));
        assertEquals(1, listCapacity.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void exceptionSetTest() {
        list.set(20, "NewElement");
    }

    @Test
    public void bigListTest() {
        for (int i = 1; i <= 20; i++) {
            list.add("" + i);
        }

        assertEquals("4", list.get(3));
        assertEquals("8", list.get(7));
        assertEquals("16", list.get(15));
    }

    @Test
    public void addTest() {
        list.add(FIRST);
        list.add(SECOND);
        list.add(THIRD);
        assertEquals("First", list.get(0));
        assertEquals("Second", list.get(1));
        assertEquals("Third", list.get(2));
    }

    @Test
    public void setTest() {
        list.add(FIRST);
        list.add(SECOND);
        assertEquals("First", list.get(0));
        list.set(0, "Orange");
        assertEquals("Orange", list.get(0));

        assertEquals(null, list.get(9));
    }

    @Test
    public void getTest() {
        list.add(FIRST);
        list.add(SECOND);
        list.add(THIRD);
        list.add(FOURTH);

        assertEquals(FIRST, list.get(0));
        assertEquals(SECOND, list.get(1));
        assertEquals(THIRD, list.get(2));
        assertEquals(FOURTH, list.get(3));
    }

    @Test
    public void indexOfTest() {
        list.add(FIRST);
        list.add(SECOND);
        list.add(THIRD);
        assertEquals(0, list.indexOf("First"));
        assertEquals(1, list.indexOf("Second"));
        assertEquals(2, list.indexOf("Third"));

        assertEquals(-1, list.indexOf("Orange"));
    }

    @Test
    public void sizeTest() {
        assertEquals(0, list.size());

        list.add(FIRST);
        list.add(SECOND);
        list.add(THIRD);
        assertEquals(3, list.size());
    }

}