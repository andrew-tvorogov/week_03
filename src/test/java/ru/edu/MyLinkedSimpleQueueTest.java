package ru.edu;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MyLinkedSimpleQueueTest {

    private static final String FIRST = "First";
    private static final String SECOND = "Second";
    private static final String THIRD = "Third";
    private static final String FOURTH = "Fourth";
    private static final String FIFTH = "Fifth";

    private SimpleQueue<String> queue;

    @Before
    public void setUp() throws Exception {
        queue = new MyLinkedSimpleQueue<>(5);
    }

    @Test
    public void offer() {
        queue.offer(FIRST);
        queue.offer(SECOND);
        assertEquals(true, queue.offer(THIRD));

        queue.offer(FOURTH);
        queue.offer(FIFTH);
        assertEquals(5, queue.size());

        assertEquals(false, queue.offer("OutOfCapacity"));
    }

    @Test
    public void poll() {
        assertEquals(null, queue.peek()); // если нет элементов

        queue.offer(FIRST);
        assertEquals(FIRST, queue.poll()); // если был один элемент
        assertEquals(0, queue.size());

        queue.offer(FIRST);
        queue.offer(SECOND);
        queue.offer(THIRD);
        assertEquals(FIRST, queue.poll()); // три элемента
        assertEquals(2, queue.size());
    }

    @Test
    public void peek() {
        assertEquals(null, queue.peek());
        queue.offer(FIRST);
        queue.offer(SECOND);
        assertEquals(FIRST, queue.peek());
    }

    @Test
    public void size() {
        assertEquals(0, queue.size());

        queue.offer(FIRST);
        queue.offer(SECOND);
        queue.offer(THIRD);
        assertEquals(3, queue.size());
    }

    @Test
    public void capacity() {
        assertEquals(5, queue.capacity());
    }
}