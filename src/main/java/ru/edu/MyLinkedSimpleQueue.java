package ru.edu;

public class MyLinkedSimpleQueue<T> implements SimpleQueue<T> {

    /**
     * Первый элемент очереди.
     */
    private Node<T> head;

    /**
     * Последний элемент очереди.
     */
    private Node<T> tail;

    /**
     * Размер очереди.
     */
    private int size;

    /**
     * Максимальный размер очереди.
     */
    private int capacity;

    /**
     * "Контейнер" для узлов(элементов) списка.
     *
     * @param <T>
     */
    private class Node<T> {
        /**
         * Ссылка на предыдущий элемент.
         */
        Node prev;

        /**
         * Содержимое элемента.
         */
        T value;

        /**
         * Ссылка на следующий элемент.
         */
        Node next;

        /**
         * Конструктор.
         *
         * @param nodeValue
         */
        Node(final T nodeValue) {
            this.value = nodeValue;
        }
    }

    /**
     * Инициализация - очередь с заданным количеством элементов.
     *
     * @param queueCapacity Количество элементов.
     */
    public MyLinkedSimpleQueue(final int queueCapacity) {
        this.capacity = queueCapacity;
    }

    /**
     * Добавление элемента в конец очереди.
     *
     * @param value элемент
     * @return true - если удалось поместить элемент (если есть место в очереди)
     */
    @Override
    public boolean offer(final T value) {
        if (size < capacity) {
            Node<T> node = new Node<>(value);
            if (head == null) {
                head = node;
            } else {
                node.prev = tail;
                tail.next = node;
            }
            tail = node;

            size++;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Получение и удаление первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T poll() {
        if (size == 1) {
            Node<T> oldHead = head;
            head = null;
            tail = null;
            size--;
            return oldHead.value;
        }
        if (size > 1) {
            Node<T> oldHead = head;
            head = head.next;
            head.prev = null;
            size--;
            return oldHead.value;
        } else {
            return null;
        }
    }

    /**
     * Получение БЕЗ удаления первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T peek() {
        if (size > 0) {
            return head.value;
        } else {
            return null;
        }
    }

    /**
     * Количество элементов в очереди.
     *
     * @return количество элементов
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Количество элементов которое может уместиться в очереди.
     *
     * @return -1 если не ограничено (будет расширяться),
     * либо конкретное число если ограничено.
     */
    @Override
    public int capacity() {
        return capacity;
    }
}
