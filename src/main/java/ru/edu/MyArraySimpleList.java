package ru.edu;

public class MyArraySimpleList<T> implements SimpleList<T> {

    /**
     * По умолчанию массив на 10 элементов.
     */
    private final int defaultSize = 10;

    /**
     * Базовый массив листа.
     */
    private T[] arr;

    /**
     * Размер листа.
     */
    private int size;

    /**
     * Конструктор по умолчанию.
     */
    public MyArraySimpleList() {
        this.arr = (T[]) new Object[defaultSize];
    }

    /**
     * Инициализация - массив с заданным количеством элементов.
     *
     * @param capacity Количество элементов в списке.
     */
    public MyArraySimpleList(final int capacity) {
        this.arr = (T[]) new Object[capacity];
    }

    /**
     * Добавление элемента в конец списка.
     *
     * @param value элемент
     */
    @Override
    public void add(final T value) {
        if (size + 1 == arr.length) {
            T[] old = arr;
            arr = (T[]) new Object[arr.length * 2];
            for (int i = 0; i < old.length; i++) {
                arr[i] = old[i];
            }
            // arr = Arrays.copyOf(arr, arr.length * 2); // оптимизировано
        }
        arr[size++] = value;
    }

    /**
     * Установка значения элемента по индексу.
     *
     * @param index индекс
     * @param value элемент
     */
    @Override
    public void set(final int index, final T value) {
        if (checkBounds(index)) {
            arr[index] = value;
        } else {
            throw new IllegalArgumentException("Индекс вне пределов массива!");
        }
    }

    /**
     * Получение элемента из списка.
     *
     * @param index индекс
     * @return значение элемента или null
     */
    @Override
    public T get(final int index) {
        if (checkBounds(index)) {
            return arr[index];
        } else {
            return null;
        }
    }

    /**
     * Удаление элемента по индексу.
     * При удалении происходит сдвиг элементов влево, начиная с index+1 и далее.
     *
     * @param index индекс
     */
    @Override
    public void remove(final int index) {
        if (checkBounds(index)) {
            if (index == 0) { // для случая, если первый элемент
                T[] tempArr = arr;
                arr = (T[]) new Object[tempArr.length];
                for (int i = 0; i < tempArr.length - 1; i++) {
                    arr[i] = tempArr[i + 1];
                }
                size--;
                return;
            }

            if (index == size - 1) { // для случая, если последний элемент
                arr[size - 1] = null;
                size--;
                return;
            }

            // для остальных случаев
            T[] oldArr = arr;
            arr = (T[]) new Object[size];
            for (int i = 0; i < size; i++) {
                if (i < index) {
                    arr[i] = oldArr[i];
                }
                if (i > index) {
                    arr[i] = oldArr[i + 1];
                }
            }
            size--;
            return;
        } else {
            throw new IllegalArgumentException("Индекс вне пределов массива!");
        }
    }

    /**
     * Получение индекса элемента по его значению.
     *
     * @param value элемент
     */
    @Override
    public int indexOf(final T value) {
        for (int i = 0; i < size; i++) {
            if (arr[i].equals(value)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Получение размера списка(количество элементов).
     *
     * @return размер списка
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Проверяет есть ли запрошенный индекс в массиве.
     *
     * @param index Индекс элемента в массиве.
     * @return true, если такой индекс существует, иначе Exception
     */
    private boolean checkBounds(final int index) {
        return  (index > size - 1 || index < 0) ? false : true;
    }
}
