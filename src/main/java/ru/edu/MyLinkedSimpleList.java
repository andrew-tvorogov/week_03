package ru.edu;

public class MyLinkedSimpleList<T> implements SimpleList<T> {
    /**
     * Первый элемент списка.
     */
    private Node<T> head;

    /**
     * Последний элемент списка.
     */
    private Node<T> tail;

    /**
     * Размер списка.
     */
    private int size;

    /**
     * "Контейнер" для узлов(элементов) списка.
     *
     * @param <T>
     */
    private class Node<T> {
        /**
         * Ссылка на предыдущий элемент.
         */
        Node prev;

        /**
         * Содержимое элемента.
         */
        T value;

        /**
         * Ссылка на следующий элемент.
         */
        Node next;

        /**
         * Конструктор.
         *
         * @param nodeValue
         */
        Node(final T nodeValue) {
            this.value = nodeValue;
        }
    }

    /**
     * Добавление элемента в конец списка.
     *
     * @param value элемент.
     */
    @Override
    public void add(final T value) {
        Node<T> node = new Node<>(value);
        if (head == null) {
            head = node;
        } else {
            node.prev = tail;
            tail.next = node;
        }
        tail = node;

        size++;
    }

    /**
     * Установка значения элемента по индексу.
     *
     * @param index индекс
     * @param value элемент
     */
    @Override
    public void set(final int index, final T value) {
        Node<T> node = head;
        for (int i = 0; i <= index; i++) {
            if (i == index) {
                node.value = value;
            } else {
                node = node.next;
            }
        }
    }

    /**
     * Получение элемента из списка.
     *
     * @param index индекс
     * @return значение элемента или null
     */
    @Override
    public T get(final int index) {
        Node<T> node = null;

        if (index == 0) {
            return head.value;
        }

        if (index == size - 1) {
            return tail.value;
        }

        if (index < size / 2) {
            node = head;
            for (int i = 0; i < index; i++) {
                node = node.next;
            }
        } else {
            node = tail;
            for (int i = 0; i < size - index - 1; i++) {
                node = node.prev;
            }
        }
        return node.value;
    }

    /**
     * Удаление элемента по индексу.
     * При удалении происходит сдвиг элементов влево, начиная с index+1 и далее.
     *
     * @param index индекс
     */
    @Override
    public void remove(final int index) {
        if (size == 1) {
            head = null;
            tail = null;
            size = 0;
            return;
        }

        if (index == size - 1) {
            tail = tail.prev;
            tail.next = null;
            size--;
            return;
        }

        if (index == 0) {
            head = head.next;
            head.prev = null;
            size--;
            return;
        }

        Node<T> node = head; // указатель на 1й элемент
        for (int i = 0; i <= index; i++) {
            if (i == index) {
                Node<T> nextNode = node.next; // хранит следующий
                Node<T> prevNode = node.prev; // хранит предыдущий
                if (prevNode == null) {
                    head = node.next;
                    head.prev = null;
                } else if (nextNode == null) {
                    tail = node.prev;
                    tail.next = null;
                } else {
                    node = prevNode;
                    node.next = nextNode;
                    nextNode.prev = node;
                }
                size--;
            } else {
                node = node.next;
            }
        }
    }

    /**
     * Получение индекса элемента по его значению.
     *
     * @param value элемент
     */
    @Override
    public int indexOf(final T value) {
        Node<T> node = head;
        for (int i = 0; i < size; i++) {
            if (node.value.equals(value)) {
                //System.out.println(i);
                return i;
            } else {
                node = node.next;
            }
        }
        return -1;
    }

    /**
     * Получение размера списка(количество элементов).
     *
     * @return размер списка
     */
    @Override
    public int size() {
        return size;
    }
}
