package ru.edu;

public class MyArraySimpleQueue<T> implements SimpleQueue<T> {

    /**
     * Базовый массив очереди.
     */
    private T[] arr;

    /**
     * Фактический размер очереди.
     */
    private int size;

    /**
     * Максимальный размер очереди.
     */
    private int queueCapacity;

    /**
     * Инициализация - очередь с заданным количеством элементов.
     *
     * @param capacity Количество элементов.
     */
    public MyArraySimpleQueue(final int capacity) {
        this.queueCapacity = capacity;
        this.arr = (T[]) new Object[capacity];
    }

    /**
     * Добавление элемента в конец очереди.
     *
     * @param value элемент
     * @return true - если удалось поместить элемент (если есть место в очереди)
     */
    @Override
    public boolean offer(final T value) {
        if (size < arr.length) {
            arr[size] = value;
            size++;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Получение и удаление первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T poll() {
        if (size > 0) {
            T element = arr[0];
            for (int i = 0; i < size - 1; i++) {
                arr[i] = arr[i + 1];
            }
            arr[size - 1] = null;
            size--;
            return element;
        } else {
            return null;
        }
    }

    /**
     * Получение БЕЗ удаления первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T peek() {
        if (size > 0) {
            return arr[0];
        } else {
            return null;
        }
    }

    /**
     * Количество элементов в очереди.
     *
     * @return количество элементов
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Количество элементов которое может уместиться в очереди.
     *
     * @return -1 если не ограничено (будет расширяться),
     * либо конкретное число если ограничено.
     */
    @Override
    public int capacity() {
        return queueCapacity;
    }
}
